# Booster 

Um projeto pensado para melhorar o integração entre empresas que cuidam do meio ambiente e a sociedade.

## Ferramentas indicadas
### Visual Studio Code
- Excelente editor de texto.
- Há a possibilidade de instalação de diversas extensões que agragarão bastante para seu desenvolvimento.

### Algumas extensões utilizadas no VSCode
- Drácula (Para estilização da aparência - fica mais escuro)
- Material Icon Theme (Estiliza a apresentação dos pacotes a depender do tipo de arquivo na aba de exploração)

## Tecnologias utilizadas

### Frontend - React.js

- Lançado em 2013 pelo Facebook;
- É uma biblioteca muito popular de javaScript utilizada para desenvolvimento de interfaces gráficas para o usuário;
- Tudo que aparecer em tela seja executado por um javascript
- Utiliza o JSX que combina javaScript com XML. Isso facilita muito a maneira de se desenvolver;
    - Sintaxe de XML dentro do javascript.
- Melhora a velocidade, pois não atualiza o DOM (Document Object Model) real a todo momento. É criada uma virtual DOM que fica em memória;
- É utilizada por grandes empresas como Netflix, Airbnb, American Express, Facebook, WhatsApp, eBay e Instagram.
- Utilizada para a criação de Single-Page Applications (SPA)
    - Em uma SPA, quando mudamos de uma visualização para outra, não há a necessidade de recarregar toda a tela. Somente os componentes necessários serão recarregados.
    - Isso melhora muito a performance da aplicação.
- Permite uma melhor organização do código
    - Criação de componentes reutilizáveis (Ex: botões, caixa de listagem de comentários).
    - Por isso falamos que o React é uma tecnologia que permite a reusabilidade de componentes;
- Permite a divisão de responsabilidades
    - Ficará responsável somente pela interface.
    - Nosso backend ficará responsável somente pelas regras de negócio.

#### O que teremos no nosso frontend
```
O desenvolvimento de nossas páginas utilizando o JSX que mescla o HTML com o JavaScript, bem como a estilização através do CSS.
Acessaremos nossas rotas da api REST para buscar os dados necessários para serem apresentados aos usuários.
```

--- 

### TypeScript
- Iremos utilizar o typeScript, pois ele nos fornece algumas facilidades em relação ao desenvolvimento.
    - Intellisense
    - Facilidade de entendimento do código pela equipe, por seguir um padrão.
    - O mercado está usando cada vez mais o typeScript.
- É important deixar claro que o TypeScript não substitui o JavaScript. Na verdade aquele foi feito baseado neste.
    - O código feito em typeScript será convertido para JS quando for executado.

# Criando o projeto react

### npx create-react-app --template=typescript
- Permite a criação de um projeto React para trabalhar com typscript (TSX).

### Estrutura do React
- Ele basicamente cria três arquivos principais.

#### src/index.tsx
- Faz a ligacao com o ReactDOM e fala para colocar todo o código html dentro da tag root que fica dentro do arquivo App.tsx.
    
#### public/index.html
- Monta o nosso html componente base que nada mais é do que uma div root.

#### src/App.tsc
- Vai centralizar todos os componentes.

# Componentização
- Muitas vezes em um site utilizamos o mesmo componentes em telas distintas. 
- O React nos permite criar componentes e reaproveitá-los onde for necessário.
- Ganho de produtividade e redução de falhas.
- Abstração de funcionalidades.

## Padrão na criação dos componentes
- Todo componente deve começar com letra maiúscula. Ex: Header.

## Propriedades de um componente
- Você pode criar um componente em formato de arrow funciton.
- Dessa forma, é possível defini-lo como do tipo Generics recebendo uma interface de propriedades.

## Estados de um componente
- Quando desejamos que as alterçaões feitas em tempo real reflitam na tela imediatamente.
- Exemplo: Clicar em aumentar e o número ser incrementado.
- O valor do estado poderá ser refletido em diversos componentes da aplicação.

### Imutabilidade
- No React não conseguimos mudar diretamente o valor de um estado.
- Para isso utilizamos o useState que irá fazer o gerenciamento de um determinado estado.
- Garante uma melhoria na performace da nossa aplicação.

### useState
- Um array [valor do estado, função para atualizar o valor do estado]

## React-router-dom
### Route do react-router-dom
- O utilizamos para criar um link entre nossos componentes e uma rota de acesso específica a depender do tipo de roteamento.
- Vamos usar o BrowserRouter que é utilizado para acessar de acordo com customizações feitas na url. Ex: localhost:3000/create-point
- Contudo, quando mudamos de componente o browser ainda estará carregando toda página.
- Para ativar realmente a característica de SPA vamos utilizar o a dependência 'Link' do react-roter-dom.

### Link do react-router-dom
- Aplicaremos a tag <Link> no lugar das tags <a>
- Dessa forma, nossa aplicação ficará mais performática, pois ao mudar de um componente para o outro somente será carregado o que for diferente da tela anterior. Não há a necessodade de recarregar toda a página novamente.
- Com isso conseguimos criar a nossa aplicação com a caracterísitica de Single Page Application.

# Integração de um mapa com o React
## React Leaflet
- Link: https://react-leaflet.js.org/
- Vamos utilizar esse componente para utilização de um mapa dentro da nossa aplicação.
- Precisamos de duas bibliotecas:
    - leaflet
    - react-leaflet
- Adiconar o css dentro do nosso index.html: https://leafletjs.com/examples/quick-start/

# Integração como a nossa API - backend
## Axios
- Vamos utilizar o axios para realizar as requisições HTTP à nossa API.
- Permite que criemos uma baseUrl que vai se repetir para todas as requisições.

## UseEffect
- Permite-nos configurar a aplicação de forma que quando uma resposta for recebida de uma requisição a função do nosso componente não tenha que ser criada novamente.
- Melhora nossa performance.
- Dois parâmetros:
    - Uma arrow function.
    - Quando devemso chamá-la: array;
        - Se esse array ficar vazio, significa que vamos chamar a função do primeiro parâmetro uma única vez.
            - Dessa forma conseguimos executar essa função somente uma vez e assim que o componente é criado, independentemente de quantas vezes este mude ou tenha algum estado alterado.
- IMPORTANTE: não permite o uso de async await. Bloqueio feito por seus criadores.
    - Nesse caso utilizaremos as promisses.

## UseState
- Utilize o useState para armazenar a resposta da requisção como estado do componente.
- Sempre que criarmos um estado para um array ou um objeto temos de informar manualmete o tipo.
    - Vamos usar a intarface para fazer uma representação do meu tipo a ser criado.

## UseRoute @react-navigation/native

# React dropzone
https://github.com/react-dropzone/react-dropzone
Vamos utilizá-lo para criar uma caixa de drag and drop.

# Utilizando a api do IBGE
## Rotas
- UFs: https://servicodados.ibge.gov.br/api/v1/localidades/estados
- Munícipios por UF: https://servicodados.ibge.gov.br/api/v1/localidades/estados/{UF}/municipios

# Notas
## TypeScript React Cheat Sheet
https://github.com/typescript-cheatsheets/react-typescript-cheatsheet

