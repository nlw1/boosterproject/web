const dev = {
    NAME: "Development Mode",
    BASE_URL: "http://localhost:3333",
};
  
const staging = {
    NAME: "Staging Mode",
    BASE_URL: "http://staging.ecoleta.org",
};

const prod = {
    NAME: "Production",
    BASE_URL: "http://ecoleta.org",
};

const config =
    process.env.REACT_APP_STAGE === "production"
        ? prod
    : process.env.REACT_APP_STAGE === "staging"
        ? staging
    : dev;

export default {
// Add common config values here
MAX_ATTACHMENT_SIZE: 5000000,
...config
};
