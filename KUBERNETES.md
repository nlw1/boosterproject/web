# Uma estrutura kubernetes

![alt text](https://kubernetes.io/images/kubernetes-horizontal-color.png)

# [Kubernetes](https://kubernetes.io/)
* Uma ferramenta OpenSource que faz a gestão de aplicações em containers através de recursos como deployments, updates, scaling e lifecycles.
* É mantido pela [Cloud Native Computing Foundation](https://www.cncf.io/)

# kubelet
* É uma engine interna na qual não precisamos mexer.
* Irá ficar supervisionando os containers que estão sendo executados nos pods do cluster.
* É o componente de nível mais baixo do Kubernetes.

# kubeadm
* É a ferramenta que irá possibilitar a instalação do kubernetes e a criação do cluster.

# kubectl
* É o client para o kubernetes.
* Vai nos propiciar executar os comandos do kubernetes na linha de comando.

# Breve explicação de cada parte de um cluster kubernetes

![alt text](https://fabriciosanchez.azurewebsites.net/3/wp-content/uploads/2017/06/kub-post3-0.png)

## Namespaces
* É a segregação lógica do cluster.
* Podemos criar vários ambientes: Staging e Production.
* Tudo irá funcionar dentro de um namespace específico.
* Os recursos do kubernetes nascem no namespace kube-system.
* Caso nenhum namespace específico seja criado tudo que criarmos será enviado a um namespace 'default'.

## Deployment
* Solicita ao kubernetes que provisione um espaço no cluster para instanciar uma imagem.
* No deploy ficam as configurações para criação das replicas de nossas imagens em pods.
* Ou seja, um Deployment cria um replica set.
* Em resumo, o deploy é uma solicitação para criação de um pod.

## ReplicaSet
* Vai ficar 'olhando' os pods criados. 
* Se tiver 3 replicas, por exemplo, se o replicaSet perceber que só tem duas irá criar a terceira novamente.
* Não temos que nos preoucupar em manter ou criar um replicaSet. Elas são controladas pelo Deployment.

## Pods
* É a engine que irá executar um ou mais containeres.
* Geralmente se tem somente um container dentro de um pod. 
* Pode-se ter mais, por exemplo, em uma aplicação que tenha que subir junto de um BD.

## Service
* O comando padrao 'kubctl expose name_deploy' cria um Service automaticamente.

### Tipos de Service
    
#### ClusterIP    
* Por padrao o Service é desse tipo.
* Esse tipo de service cria um ip para que os pods possam ser acessados por outros Services dentro do Cluster.

#### Load Balancer
* Também vai criar um ip para os pods.
* Contudo, se eu tiver usando um cloud provider ele fará um bind com essa cloud.
* Criará um ip externo para acesso.

#### NodePort
* Assim como os outros, também cria um ip interno.
* Cria uma porta que será acessível externamente pelo ip do nosso cluster.

## ConfigMap
* Um ConfigMap estará atralado a um namespace. Dessa forma podemos utilizar as váriaveis de ambiente adequada para cada ambiente específico.
* Assim podemos usar o mesmo deployment para um ambiente de Staging e Produção.



# Instalação - UBUNTU 18.04.4 LTS

    sudo su
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb https://apt.kubernetes.io/ kubernetes-xenial main
    EOF
    apt-get update
    sudo apt-get install -y kubelet kubeadm kubectl
    apt-mark hold kubelet kubeadm kubectl
    exit

# Inicializando nosso Cluster Kubernetes
* **IMPORTANTE:**
    - Desativar a memória swap:
        - UBUNTU: **swapoff -a**
* Execute o comando --> kubectl cluster-info
  - perceba que não há conexão.
* Agora execute:
  - sudo **kubeadm init** --pod-network-cidr=10.244.0.0/16

## Configurando o .kube/config padrão
* Execute os comandos solicitados na instação do cluster
  - mkdir -p $HOME/.kube
  - sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config *(Esse arquivo fala onde está o nosso server e guarda a chave de acesso)*
  - sudo chown $(id -u):$(id -g) $HOME/.kube/config *(Dar permissão para o usuário ubuntu)*

## Verificando se o cluster está funcionando
* Execute o comando --> **kubectl cluster-info**
  - perceba que nosso nó master foi criado.
* **Aqui já temos nosso cluster funcionando**.

## Supervisionando o cluster
* Abra um terminal avulso e execute:
  - **watch kubectl get all --all-namespaces** 
* Dessa forma, pode-se acompanhar o funcionamento do cluster via linha de comando.
* A cada 2s ele gera um novo status do kubernetes para você.

## Configurando nosso Pod network Addon
* Vai permitir que o cluster tenha as configurações corretas de uma rede.
* O minikube, por exemplo, já vem com essas configurações. Contudo, como estamos fazendo uma instação completa do kubernetes, faremos assim.
* **kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml**
* Agora ao executar o comando kubectl get nodes perceba que o node master está como "Ready".

## Habilitando o node master
* Por padrão o cluster kubernetes não permite que nada seja executado dentro node master.
* Há a necessidade de se ter pelo menos um worker.
* Como estamos em um ambiente de testes, habilitaremos o master para executar nossos pods.
* Execute:
  - **kubectl taint nodes --all node-role.kubernetes.io/master-**


## Kubernetes Cluster
![alt text](https://d33wubrfki0l68.cloudfront.net/152c845f25df8e69dd24dd7b0836a289747e258a/4a1d2/docs/tutorials/kubernetes-basics/public/images/module_02_first_app.svg)

* Iremos utilizar o node master para fazer o deployment de uma imagem e criar nosso primeiro serviço.

# Dashboard
### Habilitando um dashboard para uma UI com mais recursos que o terminal.
* kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

### target-port: É a porta criada para acessar nosso pod
* Descobrindo-a:
    - kubectl describe pod/kubernetes-dashboard-975499656-7pj9q -n kubernetes-dashboard
    

### port: é a porta para acessar o service - 443
* kubectl get all --all-namespaces
* Verificar a porta do service/kubernetes-dashboard


### Exposição do Dashboard
* port:nodePort -> Permite que o serviço seja acessado externamente ao cluster.
* Nosso dashboard está criado com um service de ClusterIP. Esse tipo, como será explicado mais a frente, cria um IP interno para o pod ser acessado dentro do Cluster. Contudo não é criado um ip para acesso externo ao serviço.
* Dessa forma iremos expor uma nodePort para o serviço ser acessado externamente ao cluster.
    -  **kubectl expose deployment kubernetes-dashboard --name=kubernetes-dashboard-nodeport --port=443 --target-port=8443 --type=NodePort -n kubernetes-dashboard**

### Acessando o Dashboard
* kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | sls admin-user | ForEach-Object { $_ -Split '\s+' } | Select -First 1)
* Copie o token e cole na página de login do dashboard
* Iremos acessá-lo, contudo ainda haverá erros de permissao.

# Criando um usuário com permissões de admin no Dashboard 
#### Criar um serviceAccount kubeAdmin (Ou qualquer outro nome) e dar permissão para o usuário cluster-admin

    kubectl create serviceaccount <nome qualquer. ex:kube-admin> -n kubernetes-dashboard 
    kubectl create clusterrolebinding <nome qualquer. ex:kube-admin-biding> --clusterrole=cluster-admin(admin do cluster) --serviceaccount=<namespace>:<serviceaccount>(No caso: kubernetes-dashboard:kube-admin)

#### Decodificar o token do novo usuário para acessar o Da

    kubectl describe sa kubeadmin -n kube-system
    kubectl get secret <TOKEN-ID> -n kube-system -o yaml
    echo echo <TOKEN> | base64 --decode

##### Acessando o Dashboard com permissão
* Copie o token e cole na página de login do dashboard
* Iremos acessá-lo, agora, com as permissões de admin.

# A cara do Dashboard
![alt text](https://d33wubrfki0l68.cloudfront.net/349824f68836152722dab89465835e604719caea/6e0b7/images/docs/ui-dashboard.png)


# Criando um ambiente kubernetes para teste - sem o Helm
* O objetivo nos próximos passos é mostrar a criação dos arquivos deploy e service de forma que consigamos expor nossa imagem.
* Ainda não usei o helm. Mais a frente, terá uma criação já com o helm chart.

## Ativando um docker registry local para testes
```docker run -d -p 5000:5000 --restart=always --name registry registry:2```

http://localhost:5000/v2/_catalog

## Namespace

### Verificando os namespaces
* **kubectl get ns**

### Criando novos namespaces
* Criar o um arquivo ns.yaml com o seguinte:

```
---
apiVersion: v1
kind: Namespace
metadata:
  name: staging
---
apiVersion: v1
kind: Namespace
metadata:
  name: prod
---
apiVersion: v1
kind: Namespace
metadata:
  name: devops
```

**Execute: kubectl apply -f ns.yaml**

---

### Criando o deployment

nano deployment.yaml

```
apiVersion: apps/v1
kind: Deployment
metadata: (Metadados do próprio deploy)
  name: frontend
  labels:
    app: frontend
spec: (Tudo que estiver nesse spec diz respeito ao Pod que será criado)
  replicas: 1
  selector: (Irá buscar o Pod para fazer o bind)
    matchLabels:
      app: frontend (Nome do Pod específico)
  template:
    metadata: (Metadados do Pod)
      labels:
        app: frontend
    spec: (Dados dos containers. No caso só temos 1)
      containers: 
      - name: frontend
        image: localhost:5000/frontend-cred
        ports:
        - containerPort: 8888

```

**Execute: kubectl apply -f deployment.yaml**


### Criando nosso service

nano service.yaml

```
kind: Service
apiVersion: v1
metadata:
  name: frontend
spec:
  type: NodePort
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 8888
    targetPort: 8888
    nodePort: 30088

```

**Execute: kubectl apply -f service.yaml**

### Acesse sua aplicação 

http://localhost:30080


# Instalação Helm & Tiller
![alt text](https://helm.sh/img/helm.svg)

## Instalação do heml 2
curl -L https://git.io/get_helm.sh | bash

## Helm
* É um pacote capaz de fazer o gerenciamento de uma aplicação Kubernetes.
* Assim como o kubernetes é mantido pela [Cloud Native Computing Foundation](https://www.cncf.io/)
* O helm é nosso client, assim como o kubectl para o kubernetes.

## Inicia-se o helm
* **helm init**
* Com esse comando o helm consegue fazer a referência ao kubectl.
* O kubectl mostra ao helm em qual contexto se encontra e dessa forma o tiller é instanciado.

## Tiller
* Tiller é a parte do helm que é executada dentro do kubernetes. 
* Através da aplicação de um serviceAccount o Tiller consiguirá dar acesso ao cluster.

## Service account para o Tiller
* Para que consigamos gerenciar nosso cluster kubernetes.

### São 3 passos
- Criar o service account
- Criar uma matriz de permissão
- Associar essa matriz ao service account

#### Criar um service account
- Criar um service account com o nome tiller
- Crie o arquivo: **tiller-sa.yaml**
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
```
- Execute: **kubectl apply -f tiller-sa.yaml**


#### Criar um cluster role
- Um descritivo de:
    - quais apis o service account tem acesso.
    - quais resources:
        - pods, deploy, services, secrets, configmaps ...
    - verbs (list, create, delete, get)
- Crie o arquivo: **allresources-clusterrole.yaml**
```
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: allresources
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]
```
- Execute: **allresources-clusterrole.yaml**


#### Criar um Cluster Role Biding
- Associar a service account com um cluster role
- Perceba que:
    - o 'subject' é a nossa service account;
    - e o 'roleRef' é nosso cluster role.
- Crie o arquivo: **tiller-allresources-clusterrolebiding.yaml**
```
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: tiller
subjects:
- kind: ServiceAccount
  namespace: kube-system
  name: tiller
  apiGroup: ""
roleRef:
  kind: ClusterRole
  name: allresources
  apiGroup: rbac.authorization.k8s.io
```
- Execute: **kubectl apply -f tiller-allresources-clusterrolebiding.yaml**

### Verificando nosso service account
- kubectl get sa -n kube-system | grep tiller
- kubectl describe sa tiller -n kube-system
- O tiller ainda não sabe que tem de usar esse service account. 
- Para isso vamos fazer um patch

## Patch
https://kubernetes.io/docs/tasks/manage-kubernetes-objects/update-api-object-kubectl-patch/
- Com o patch conseguimos atualizar o tiller de uma maneira global.
- Crie o seguinte arquivo:
```
spec:
  template:
    spec:
      serviceAccountName: tiller
```
- Ou seja, ao ser executado, o patch vai procurar a primeira tag spec dentrpo do arquivo de deploy.
- Depois procura por template e por fim outra tag spec.
- Acrescenta o serviceAccountName.

### Executar o patch
- Primeiro verifique que no arquivo tiller-deploy não há um serviceAccount
    - kubectl get deploy tiller-deploy -n kube-system -o yaml | grep serviceAccount
- Agora execute o patch:
    - kubectl patch deployment tiller-deploy -n kube-system --patch "$(cat tiller-patch.yaml)"
- Execute o get novamente e verifique que o arquivo foi atualizado.
    - kubectl get deploy tiller-deploy -n kube-system -o yaml | grep serviceAccount
- Verifique também o pod do tiller-deploy e veja que ele foi atualizado, agora está com as permissões.
    - kubectl get all -n kube-system

# Nota
- Assim como existem as ClusterRoles e as ClusterRoleBindings, existem também as Roles e as RoleBindings e sua diferença básica está no escopo!

## ClusterRoles e as ClusterRoleBindings
- Como o próprio nome diz, as Cluster Role e  a Cluster Role Binding são globais, atigem TODOS os namespaces! 
- Ou seja, a role atrelada ao nosso Service Account Helm tem escopo de 'cluster'! Portanto terá permissões em TODOS os namespaces!

## Roles e as RoleBindings
- São atreladas a um namespace específico. 

# Pronto.
- Agora é possível utilizar o helm e o tiller.

![alt text](https://helm.sh/img/boat.svg)
# Kickstarting
- O helm é um gerenciador de pacotes para o kubernetes.
- Vamos começar a criar toda nossa estrutura kubernetes com o helm.

# Estrutura de um chart
- https://github.com/helm/charts
```
chartname/            # Já tem de ser o nome do chart
  Chart.yaml          # Informações Técnicas
  README.md           # Opcional: manual, explicações, etc.
  values.yaml         # Variáveis e Valores
  templates/          # Modelos de Deployments, Services e todos outros recursos
```
- Como pode ser visto no link, há formatos de charts mais complexos.
- Entretento, vamos nos basear nesse modelo apresentado acima.

# Criando nosso chart para o frontend
## Helm create
- helm create frontend
- Esse comando vai criar os arquivos com a estrutura necessária para o kubernetes.

## Vou colocar aqui como ficou inicialmente três arquivos importantes:

### values.yaml
- Armazenará todos nossos valores que serão referenciados nos outros arquivos.
```
# Default values for frontend-ecoleta.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: localhost:5000/frontend-ecoletado
  tag: 1.1.0
  pullPolicy: IfNotPresent

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: NodePort
  port: 80
  nodePort: 30080

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []

  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}

```

### Chart.yaml
```
apiVersion: v1
appVersion: "1.0"
description: A Helm chart for Ecoleta frontend
name: frontend-ecoleta
version: 0.1.0
```

### deplyment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "frontend-ecoleta.fullname" . }}
  labels:
{{ include "frontend-ecoleta.labels" . | indent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "frontend-ecoleta.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "frontend-ecoleta.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ template "frontend-ecoleta.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}

```

# Install helm
- Dentro da pasta onde está o seu chart execute: helm install . --name frontend-ecoleta
- Qualquer erro use: helm delete frontend-ecoleta --purge
- Feito isso ajuste os erros e crie novamente.

## Acessando a aplicação
- Para acessar: localhost:30180
- Sua aplicação já está acessível em um cluster kubernetes.


# Instalação Chart Museum
![alt text](https://raw.githubusercontent.com/helm/chartmuseum/master/logo2.png)
https://github.com/helm/chartmuseum

- Vamos utilizá-lo para criar um novo repositório local.
- Crie um arquivo chartmuseum-conf.yaml
```
env:
  open:
    STORAGE: local
    DISABLE_API: false
    ALLOW_OVERWRITE: true
service:
  type: NodePort
  nodePort: 30010
```
- Excute: helm install --name helm --namespace devops -f chartmuseum-conf.yaml stable/chartmuseum
- Verifique: localhost:30010

## Criando nosso repositório
- Por padrão o helm só tem o repositório stable.
- Vamos criar agora o ecoleta.
    - **helm repo add ecoleta http://$(kubectl get nodes --namespace devops -o jsonpath="{.items[0].status.addresses[0].address}"):30010**

## Instalação do helm-push
- Esse plugin é capaz de enviar um chart a um repositório de uma maneira rápida (a maneira lenta seria montar e disparar um POST):
    - helm plugin install https://github.com/chartmuseum/helm-push

## Populando nosso repositório
- helm lint chart/frontend-ecoleta/ (verficar se está correto nosso arquivo)
- helm push chart/frontend-ecoleta/ ecoleta

## Update and Search
- Atualize os repositórios: helm repo update
- Pesquise para ver que o repositório foi populado com o chart: helm search

# Up & Running via Helm e ChartMuseum
- Primeiro deletar todos os charts exceto o helm (chartmuesum)
- Subir tudo novamente agora direto do repositório
    - helm install ecoleta/frontend-ecoleta --namespace staging --name staging-frontend-ecoleta
- Pode testar o acesso na nodePort.

# Push de uma nova versão da imagem da nossa aplicação.
- Ao ser publicada uma nova versão de nossa aplicação, precisamos atualizá-la no kubernetes.
- Para isso usamos o upgrade:
    - helm upgrade staging-frontend-ecoleta ecoleta/frontend-ecoleta --set image.tag=1.2.0

## Alta disponibilidade
- Ao executar o coamando de upgrade o kubernetes vai gerar um novo pod para nossa aplicação e só vai apagar o antigo quando o novo estiver criado.
- Dessa forma a aplicação não fica fora do ar durante a transição.

## Voltando a uma versão anterior
- No caso de se perceber um erro ou qualquer outra necessidade, pode-se voltar para uma versão anterior.
- Execute: helm ls
    - Verifique em qual revisão do chart você está e para qual quer voltar
- Feito isso execute:
    - helm rollback staging-frontend-ecoleta 1