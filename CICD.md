![alt text](https://miro.medium.com/max/1352/1*hE_6eVlhsJ2X63lF2pXj0A.png) 

# O que é?
- É uma ferramenta de integração contínua de código aberto
- Mantido pela CDF - Continuous Delivery Foundation.

![alt text](https://cdn.shopify.com/s/files/1/0082/9879/0963/files/cdf-horizontal-color_360x.jpg?v=1557688897)

https://cd.foundation/

---

# Integração e Entrega Contínua
![alt](https://miro.medium.com/max/1400/1*iKuaNfxgZSTe_J2x3PYRUg.png)

## Integração contínua
- É a técnica utilizada para que vários desenvolvedores possam trabalhar em uma mesma aplicação de forma dinâmica e confiável.
- Trabalhamos com branchs (ramificações), em que há a principal, master, a qual representa um estado de pronto.
- Teremos também uma branch de desenvolvimento que dára origem as releases que serão, posteriormente, incorporadas à master.
- Uma metodologia que eu utilizo e é bastante utilizada no mercado é o gitflow. Posteriormente farei um tutorial sobre ele.
- Na integração contínua, a meta é que os desenvolvedores estejam sempre realizando commits de código e enviando para o repositório para que possam falhar o mais cedo possível.
- Essa integração constante de código, faz com que bugs sejam descobertos mais cedo e que a união do código dos vários desenvolvedores falhem o quanto antes para que seja resolvido.
- O uso de uma ferramenta como Jenkins, na integração contínua, contribui para:
    - Feedback rápido;
    - Acesso segmentado;
    - Históricos e relatórios;
    - Facilidade em gerir o ciclo de desenvolvimento;
    - Automação de atividades manuais;
- É importante saber que o Jenkins não deve ser utilizado para a entrega contínua. Ele não tem essa finalidade. Para isso usaremos outras ferramentas, como apresentado mais à frente.

# Instalação do Jenkins 
- O Jenkins vai ser instanciado em nosso namespace devops
- Para que, caso ocorra algum crash no cluster, possamos recuperar nossos dados vamos criar um volume local para armazenar os dados do Jenkins.

## Crie um diretório para o Persistent Volume
- mkdir /home/<usuario>/mnt/data-jenkins
- chmod 777 mnt/data-jenkins

## Persistent Volume
- Um volume local para armazenar os dados e metadados do Jenkins
- Crie o arquivo: **jenkins-pv.yaml**
```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: jenkins
  labels:
    type: local
spec:
  storageClassName: manual-for-jenkins
  capacity:
    storage: 16Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "home/<username>/mnt/data-jenkins"
```
- Execute: **kubectl apply -f jenkins-pv.yaml**

## Persistent Volume Claim
- Uma abstração entre o pod do Jenkins e o Persistent Volume
- Crie o arquivo: **jenkins-pvc.yaml**
```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: jenkins
  namespace: devops
spec:
  storageClassName: manual-for-jenkins
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 16Gi
```
- Execute: **kubectl apply -f jenkins-pvc.yaml**


# Agora sim instalando o Jenkins com Helm

![alt text](https://www.jenkins.io/images/jenkins-x-logo.png)

https://github.com/helm/charts/tree/master/stable/jenkins

- Execute: **helm install --name jenkins --set persistence.existingClaim=jenkins --set master.serviceType=NodePort --set master.nodePort=31808 --namespace devops stable/jenkins**

## Para acompanhar a instalação
- cd /home/<usuario>/mnt/data-jenkins
- watch ls -ltra
- watch kubectl get all -n devops

## Aguarde a instalação e acesse
- Primeiramente decodifique a senha
    - Execute: 
        - helm status jenkins
    - Vai apresentar algo assim:
        - printf $(kubectl get secret --namespace devops jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo

## Acesse:
- http://localhost:31808
- Login: admin 
- Senha: gerada no passo anterior

![alt text](https://www.jenkins.io/images/post-images/jenkins-is-the-way/jenkins-is-the-way.png)

## Pemissões
- Por padrão o Jenkins irá usar o serviceAccount devops.
- Precisamos expandir o permissionamento desse sa para que ele consiga olhar para os outros namespaces:
    - O Jenkins irá precisar das permissões para listar, criar e excluir seus pods escaláveis no namespace devops: 
        - kubectl create rolebinding sa-devops-role-clusteradmin --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=devops
    - E, futuramente através do helm, teremos que fazer algumas ações junto ao kube-system:
        - kubectl create rolebinding sa-devops-role-clusteradmin-kubesystem --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=kube-system

# Primeiras credenciais Jenkins

## Criando uma credencial para conexão com o gitlab
- No menu lateral do Jenkins, vá em credenciais. Depois clique em globais.
- Vamos criar uma SSH Key.

#### Crie uma ssh key local
- https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair
- ssh-keygen -o -t rsa -b 4096 -C "jenkins-gitlab"
- Coloque um nome. Ex: /home/<usuario/.ssh/id_rsa_jenkins
- Se quiser adicione uma senha
- Ok!

#### Adicione a chave pública ao gitlab
- cat /home/<usuario/.ssh/id_rsa_jenkins.pub
- Ok!

#### Adicione a chave privada ao Jenkins
- cat /home/<usuario/.ssh/id_rsa_jenkins
- Ok!

## Criando uma credencial para o Docker Hub
- Agora vamos criar uma 'userName with password'
- username e password do docker hub
- id: dockerhub
- description: hub.docker.com
- Ok!

# Criando nossa pipeline
- O que é uma pipeline?
    - É a sequência lógica desde a integração do código até a implementação.

## Criar um atualizador de versões
- Na raíz do nosso projeto vamos criar um arquivo read-package-version.sh
```
PACKAGE_VERSION=$(grep -m1 version package.json | awk -F: '{ print $2 }' | sed 's/[", ]//g')
echo $PACKAGE_VERSION | tr -d '\r'
```
- Esse script vai pegar a versão do package.json

## Criando um Service Discovery
- Precisamos de uma maneira de acessar nosso repositório chart Museum de dentro do nosso pod do jenkins.
- O kubernetes nos disponibiliza uma feature de service discovery. Ou seja, uma maneira de os services se comunicarem.
- Para entender vamos acessar o pod do jenkins.
- Execute um kubectl get po,svc -n devops e pegue o nome do pod do jenkins.
- Para entrar no pod:
    - kubectl exec -it jenkins-6779bb6f6c-p42xz sh -n devops
- Vamos localizar o serviço do chart musemum:
    - curl helm-chartmuseum:8080
- Se não estiver dentro do mesmo namespace ou cluster, pode-se usar o dns completo:
    - curl helm-chartmuseum.devops.svc.cluster.local:8080
- Então é basicamente isso que precisamos fazer para acessar o service do chart museum.
- Vamos adicionar o nosso repositório, para acessarmos os charts.

## Criando uma multbranch pipeline no Jenkins
- Vamos criar um novo item do tipo multi branch pipeline com as seguintes configurações:
    - Branch Source: Single repository & branch
        - Adicione o seu repositório
        - As credenciais que criamos para o git.
        - Deixe a Branch master que já vem por padrão.
        - Adicione a Branch develop (seguindo o padrão gitflow)
    - Build Configuration
        - Deixe o Jenkinsfile que será nosso arquivo de referência.
    Feito isso pode salvar.

## Jenkinsfile
- Agora o jenkins ao executar uma pipeline vai utilizar o Jenkinsfile do projeto como referência.
- Deixe-o dessa maneira:
```
podTemplate(
    name: 'ecoleta',
    namespace: 'devops',
    label: 'ecoleta', 
    containers: [
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', livenessProbe: containerLivenessProbe(execArgs: '', failureThreshold: 0, initialDelaySeconds: 0, periodSeconds: 0, successThreshold: 0, timeoutSeconds: 0), name: 'docker-container', resourceLimitCpu: '', resourceLimitMemory: '', resourceRequestCpu: '', resourceRequestMemory: '', ttyEnabled: true, workingDir: '/home/jenkins/agent'),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.11.0', name: 'helm-container', ttyEnabled: true,)
    ],
    volumes: [
      hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
    ]
)
{
    //Start Pipeline
    node('ecoleta') {       
        def REPOS
        def IMAGE_VERSION
        def KUBE_NAMESPACE
        def IMAGE_NAME = "frontend-ecoleta"
        def ENVIRONMENT        
        def GIT_REPOS_URL = "git@gitlab.com:nlw1/boosterproject/web.git"
        def GIT_BRANCH
        def HELM_DEPLOY_NAME
        def HELM_CHART_NAME = "ecoleta/frontend-ecoleta"
        def CHARTMUSEUM_URL = "http://helm-chartmuseum.devops.svc.cluster.local:8080"
        def REPO_DOCKER_LOCAL = "localhost:5000"

        stage('Checkout') {
            echo 'Iniciando Clone do Repositório'
            REPOS = checkout([$class: 'GitSCM', branches: [[name: '*/master'], [name: '*/develop']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: GIT_REPOS_URL]]])            
            GIT_BRANCH = REPOS.GIT_BRANCH
            echo REPOS
            if(GIT_BRANCH.equals("origin/master")){
                KUBE_NAMESPACE = "prod"
                // package.json
                ENVIRONMENT = "production"
            } else if(GIT_BRANCH.equals("origin/develop")){
                KUBE_NAMESPACE = "staging"
                ENVIRONMENT = "staging"
            } else {
                def error = "Não existe pipeline para a branch ${GIT_BRANCH}"
                echo error
                throw new Exception(error)
            }
            HELM_DEPLOY_NAME = KUBE_NAMESPACE + "-frontend-ecoleta"
            IMAGE_VERSION = sh returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim()
        }
        stage('Package') {
            container('docker-container') {
                echo 'Iniciando Empacotamento com Docker'                
                sh "docker build -t ${REPO_DOCKER_LOCAL}/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV='${ENVIRONMENT}'"
                sh "docker push ${REPO_DOCKER_LOCAL}/${IMAGE_NAME}:${IMAGE_VERSION}"                                
            }                                    
        }
        stage('Deploy') {
            container('helm-container') {
                echo 'Iniciando Deploy com Helm'                
                sh """
                    helm init --client-only
                    helm repo add ecoleta ${CHARTMUSEUM_URL}
                    helm repo update                    
                """
                try {
                    // Fazer helm upgrade
                    sh "helm upgrade --namespace=${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION}"
                } catch(Exception e){
                    // Fazer helm install
                    sh "helm install --namespace=${KUBE_NAMESPACE} --name ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION}"
                }
            }
        }   
    }//end of node
}
```
- O que é feito nesse script?
    - Pedimos que o kubernetes crie um pod para executar a nossa pipeline.
    - Observe que solicitamos a criação de um container docker para que possamos gerar e publicar nossa imagem.
    - Da mesma forma que criamos um container com a imagem oficial do docker, para gerarmos nosso build e publicar nossa imagem.
        - Vamos precisar de uma imagem em que consigamos utilizar o helm.
            - Utilizamos a seguinte imagem: lachlanevenson/k8s-helm
- Criamos 3 estágios dentro da nossa pipeline.
    - **Checkout:**
        - Acessar o gitlab com as credenciais que criamos.
        - Fazer o clone do repositório.
        - Verificar qual a branch que estamos e setar o ambiente correto.
        - Setamos o nome do nosso helm deploy.
        - E a versão da imagem.
    - **Package:**
        - Usamos o container docker dentro do nosso pod.
        - Geramos um build da nossa aplicação.
        - Publicamos a imagem no docker registry local.
    - **Deploy:**
        - Acessa o service do nosso repositório através do HELM:
            - Utilizamos o recurso de Service Discovery para acessar o repositório.
        - Dessa forma, é feito o deploy no kubernetes.
        - Nossa aplicação está acessível